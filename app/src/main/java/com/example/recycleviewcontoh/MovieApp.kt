package com.example.recycleviewcontoh

import android.app.Application
import com.example.recycleviewcontoh.di.viewModelModule
import com.training.pagingsample.di.networkModule
import com.training.pagingsample.di.repositoryModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Dhimas Saputra on 07/01/21
 * Jakarta, Indonesia.
 */
class MovieApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin{
            androidContext(this@MovieApp)
            modules(listOf(networkModule, repositoryModule, viewModelModule))
        }
    }
}