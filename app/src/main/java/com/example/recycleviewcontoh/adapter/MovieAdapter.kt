package com.example.recycleviewcontoh.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleviewcontoh.databinding.CardviewListBinding
import com.example.recycleviewcontoh.databinding.LoadStateViewBinding
import com.example.recycleviewcontoh.model.Movie
import com.example.recycleviewcontoh.model.Person

/**
 * Created by Dhimas Saputra on 06/01/21
 * Jakarta, Indonesia.
 */
class MovieAdapter :  PagingDataAdapter<Movie,RecyclerView.ViewHolder>(NoteDiffCallBack) {

    companion object NoteDiffCallBack : DiffUtil.ItemCallback<Movie>(){
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return  oldItem.id == newItem.id

        }

        override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem == newItem

        }
    }
    class ViewHolder(val binding: CardviewListBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(current: Movie) {
            binding.tvName.text = current.original_title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(CardviewListBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val current = getItem(position)
        viewHolder.bind(current!!)
    }


}