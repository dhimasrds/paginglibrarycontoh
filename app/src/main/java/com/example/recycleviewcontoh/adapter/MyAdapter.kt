package com.example.recycleviewcontoh.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.recycleviewcontoh.base.NoteEntity
import com.example.recycleviewcontoh.databinding.CardviewListBinding
import com.example.recycleviewcontoh.model.Movie
import com.example.recycleviewcontoh.model.Person

/**
 * Created by Dhimas Saputra on 29/12/20
 * Jakarta, Indonesia.
 */
class MyAdapter: PagingDataAdapter<NoteEntity,RecyclerView.ViewHolder>(NoteDiffCallBack){

    companion object NoteDiffCallBack : DiffUtil.ItemCallback<NoteEntity>(){
        override fun areItemsTheSame(oldItem: NoteEntity, newItem: NoteEntity): Boolean {
            return  oldItem.id == newItem.id

        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: NoteEntity, newItem: NoteEntity): Boolean {
            return oldItem == newItem


        }
    }
    class ViewHolder(val binding: CardviewListBinding) :RecyclerView.ViewHolder(binding.root) {

        fun bind(current: NoteEntity) {
            binding.tvName.text = current.title
            binding.tvAge.text = current.desc
            binding.tvAge.setOnClickListener {

            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):RecyclerView.ViewHolder {
        return ViewHolder(CardviewListBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as MyAdapter.ViewHolder
        val current = getItem(position)
        viewHolder.bind(current!!)
    }


}