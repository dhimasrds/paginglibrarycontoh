package com.example.recycleviewcontoh.Repo

import android.util.Log
import androidx.paging.PagingSource
import com.example.recycleviewcontoh.base.NoteEntity
import com.example.recycleviewcontoh.dao.NoteDao
import com.example.recycleviewcontoh.dao.NoteDaoImp
import com.example.recycleviewcontoh.model.Movie
import java.io.IOException

/**
 * Created by Dhimas Saputra on 06/01/21
 * Jakarta, Indonesia.
 */
private const val FIRST_INDEX = 0


class NotePagingSource(
) : PagingSource<Int, NoteEntity>() {
    private val noteDao: NoteDao
    private val repository: NoteRepository

    init {
        noteDao = NoteDaoImp()
        repository = NoteRepository(noteDao)
    }


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, NoteEntity> {
        val position = params.key ?: FIRST_INDEX
        Log.d("position", position.toString())

        return try {
            val listNote = mutableListOf<NoteEntity>()
            val list = repository.getAllNotes1(position.toLong())
            list?.let { listNote.addAll(it) }
            Log.d("size", listNote.size.toString())

            LoadResult.Page(
                data = listNote,
                prevKey = if (position == FIRST_INDEX) null else position,
                nextKey = if (position > 8 ) null else position + 3

            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        }
    }

}