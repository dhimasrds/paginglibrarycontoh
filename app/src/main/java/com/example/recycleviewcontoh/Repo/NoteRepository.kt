package com.example.recycleviewcontoh.Repo

import com.example.recycleviewcontoh.base.NoteEntity
import com.example.recycleviewcontoh.dao.NoteDao

/**
 * Created by Dhimas Saputra on 07/01/21
 * Jakarta, Indonesia.
 */
class NoteRepository(val noteDao: NoteDao) {
    fun getAllNotes1(offset : Long)  : List<NoteEntity>? = noteDao.getAllNotes1(offset)
}