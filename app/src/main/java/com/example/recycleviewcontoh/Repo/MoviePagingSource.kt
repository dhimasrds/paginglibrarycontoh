package com.example.recycleviewcontoh.Repo

import androidx.paging.PagingSource
import com.example.recycleviewcontoh.model.Movie
import java.io.IOException

/**
 * Created by Dhimas Saputra on 07/01/21
 * Jakarta, Indonesia.
 */
class MoviePagingSource (
    private val repository: MovieRepository
) : PagingSource<Int, Movie>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movie> {

        return try {
            val nextPage = params.key ?: 1
            val movieListResponse = repository.getPopularMovies(nextPage)

            LoadResult.Page(
                data = movieListResponse.results!!,
                prevKey = if (nextPage == 1) null else nextPage - 1 ,
                nextKey = if (nextPage < movieListResponse.totalPages!!)
                    movieListResponse.page?.plus(1) else null
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        }
    }

}