package com.example.recycleviewcontoh.Repo

import com.example.recycleviewcontoh.base.Result
import com.training.pagingsample.data.network.MovieAppService
import com.training.pagingsample.data.network.response.MovieListResponse

/**
 * Created by Dhimas Saputra on 06/01/21
 * Jakarta, Indonesia.
 */
class MovieRepository(private val service: MovieAppService) {

    suspend fun getPopularMovies(page: Int) : MovieListResponse {
        return when(val result = service.fetchPopularMovies(page)){
            is Result.Success -> result.data
            is Result.Error -> throw result.error
        }
    }
}