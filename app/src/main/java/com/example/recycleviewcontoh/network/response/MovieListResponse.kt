package com.training.pagingsample.data.network.response

import com.example.recycleviewcontoh.base.NoteEntity
import com.example.recycleviewcontoh.model.Movie
import com.training.pagingsample.data.network.response.base.ListResponse

class MovieListResponse : ListResponse<Movie>()