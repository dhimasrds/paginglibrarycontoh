package com.training.pagingsample.data.network

import com.example.recycleviewcontoh.base.Result
import com.training.pagingsample.data.network.response.MovieListResponse

class MovieAppService(private val api: Api) : BaseService() {

    suspend fun fetchPopularMovies(page: Int) : Result<MovieListResponse> {
        return createCall { api.getPopularMovies(page) }
    }
}