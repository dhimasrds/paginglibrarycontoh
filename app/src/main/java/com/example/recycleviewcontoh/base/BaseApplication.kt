package com.example.recycleviewcontoh.base

import android.app.Application
import com.example.recycleviewcontoh.di.viewModelModule
import com.training.pagingsample.di.networkModule
import com.training.pagingsample.di.repositoryModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Dhimas Saputra on 06/01/21
 * Jakarta, Indonesia.
 */
class BaseApplication : Application(){
    override fun onCreate() {
        super.onCreate()
        ObjectBox.init(this)

        startKoin{
            androidContext(this@BaseApplication)
            modules(listOf(networkModule, repositoryModule, viewModelModule))
        }
    }
}