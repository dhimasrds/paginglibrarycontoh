package com.example.recycleviewcontoh.base

/**
 * Created by Dhimas Saputra on 06/01/21
 * Jakarta, Indonesia.
 */
sealed class Result<out T: Any> {
    data class Success<out T: Any>(val data: T) : Result<T>()
    data class Error(val error: Exception) : Result<Nothing>()
}