package com.example.recycleviewcontoh.model

import android.widget.ImageView

/**
 * Created by Dhimas Saputra on 29/12/20
 * Jakarta, Indonesia.
 */
class Person(
    val image : Int,
    val name : String,
    val age : Int
)