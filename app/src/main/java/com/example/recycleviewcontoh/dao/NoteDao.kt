package com.example.recycleviewcontoh.dao

import com.example.recycleviewcontoh.base.NoteEntity
import io.objectbox.android.ObjectBoxLiveData

/**
 * Created by Dhimas Saputra on 07/01/21
 * Jakarta, Indonesia.
 */
interface NoteDao {
    fun save(noteEntity: NoteEntity)
    fun delete(noteEntity: NoteEntity)
    fun getAllNote(): ObjectBoxLiveData<NoteEntity>
    fun getAllNotes(): List<NoteEntity>
    fun getAllNotes1(offset: Long): List<NoteEntity>?
}