package com.example.recycleviewcontoh.dao

import android.util.Log
import com.example.recycleviewcontoh.base.NoteEntity
import com.example.recycleviewcontoh.base.NoteEntity_
import com.example.recycleviewcontoh.base.ObjectBox
import com.skydoves.whatif.whatIfNotNull
import io.objectbox.Box
import io.objectbox.android.ObjectBoxLiveData

/**
 * Created by Dhimas Saputra on 07/01/21
 * Jakarta, Indonesia.
 */
class NoteDaoImp : NoteDao {

    lateinit var noteEntityLiveData: ObjectBoxLiveData<NoteEntity>
    lateinit var noteEntityBox: Box<NoteEntity>


    override fun save(noteEntity: NoteEntity) {
        noteEntityBox = ObjectBox.boxStore.boxFor(NoteEntity::class.java)
        noteEntityBox.put(noteEntity)
    }

    override fun delete(noteEntity: NoteEntity) {
        noteEntityBox = ObjectBox.boxStore.boxFor( NoteEntity::class.java)
        noteEntityBox.remove(noteEntity)
    }

    override fun getAllNote(): ObjectBoxLiveData<NoteEntity> {
        noteEntityBox = ObjectBox.boxStore.boxFor(NoteEntity::class.java)
        noteEntityLiveData = ObjectBoxLiveData(noteEntityBox.query().notNull(NoteEntity_.title).build())
        Log.d("allNote ",noteEntityLiveData.toString())
        return noteEntityLiveData
    }

    override fun getAllNotes(): List<NoteEntity> {
        noteEntityBox = ObjectBox.boxStore.boxFor(NoteEntity::class.java)
        return noteEntityBox.query().notNull(NoteEntity_.title).build().find()
    }

    override fun getAllNotes1(offset : Long ): List<NoteEntity>? {
        noteEntityBox = ObjectBox.boxStore.boxFor(NoteEntity::class.java)
        val notesEntities: List<NoteEntity> =
            noteEntityBox.query().notNull(NoteEntity_.id).build().find(offset, 3)
        notesEntities.whatIfNotNull(whatIf = { return notesEntities })
        return null
    }

    fun deleteAll(){
        noteEntityBox = ObjectBox.boxStore.boxFor(NoteEntity::class.java)
        noteEntityBox.removeAll()
    }
}