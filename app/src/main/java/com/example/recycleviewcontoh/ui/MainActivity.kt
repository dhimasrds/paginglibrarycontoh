package com.example.recycleviewcontoh.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recycleviewcontoh.adapter.MovieAdapter
import com.example.recycleviewcontoh.adapter.MovieLoadStateAdapter
import com.example.recycleviewcontoh.adapter.MyAdapter
import com.example.recycleviewcontoh.adapter.NoteLoadStateAdapter
import com.example.recycleviewcontoh.base.NoteEntity
import com.example.recycleviewcontoh.dao.NoteDao
import com.example.recycleviewcontoh.dao.NoteDaoImp
import com.example.recycleviewcontoh.databinding.ActivityMainBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by Dhimas Saputra on 07/01/21
 * Jakarta, Indonesia.
 */
class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()

    private lateinit var movieAdapter: MovieAdapter
    private lateinit var myAdapter: MyAdapter
    private lateinit var mainBinding: ActivityMainBinding
    private lateinit var noteDao :NoteDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)

        movieAdapter = MovieAdapter()
        myAdapter = MyAdapter()
        noteDao = NoteDaoImp()

        for (i in 1..50) {
            val noteEntity = NoteEntity()
            noteEntity.title = "title $i"
            noteEntity.desc = "desc $i"

            noteDao.save(noteEntity)

        }

//        mainBinding.recyclerView.apply {
//            layoutManager = LinearLayoutManager(context)
//            setHasFixedSize(true)
//            adapter = myAdapter.withLoadStateFooter(
//                footer = NoteLoadStateAdapter { myAdapter.retry() }
//            )
//        }

        mainBinding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = movieAdapter.withLoadStateFooter(
                footer = MovieLoadStateAdapter { myAdapter.retry() }
            )
        }

//        lifecycleScope.launch {
//            viewModel.notes.collect {
//               myAdapter.submitData(it)
//            }
//        }

        lifecycleScope.launch {
            viewModel.movies.collect {
                movieAdapter.submitData(it)
            }
        }

        mainBinding.btnRetry.setOnClickListener{
           movieAdapter.retry()
        }

        // show the loading state for te first load
       movieAdapter.addLoadStateListener { loadState ->

            if (loadState.refresh is LoadState.Loading) {

                mainBinding.btnRetry.visibility = View.GONE

                // Show ProgressBar
                mainBinding.progressBar.visibility = View.VISIBLE
            }
            else {
                // Hide ProgressBar
                mainBinding.progressBar.visibility = View.GONE

                // If we have an error, show a toast
                val errorState = when {
                    loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                    loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                    loadState.refresh is LoadState.Error -> {
                        mainBinding.btnRetry.visibility = View.VISIBLE
                        loadState.refresh as LoadState.Error
                    }
                    else -> null
                }
                errorState?.let {
                    Toast.makeText(this, it.error.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}