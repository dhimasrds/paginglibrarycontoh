package com.example.recycleviewcontoh.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.example.recycleviewcontoh.Repo.MoviePagingSource
import com.example.recycleviewcontoh.Repo.MovieRepository
import com.example.recycleviewcontoh.Repo.NotePagingSource
import com.example.recycleviewcontoh.base.NoteEntity
import com.example.recycleviewcontoh.model.Movie
import kotlinx.coroutines.flow.Flow

/**
 * Created by Dhimas Saputra on 06/01/21
 * Jakarta, Indonesia.
 */
class MainViewModel(private val repository: MovieRepository) : ViewModel() {


    val movies: Flow<PagingData<Movie>> = Pager(PagingConfig(pageSize = 20)) {
        MoviePagingSource(repository)
    }.flow
        .cachedIn(viewModelScope)

    val notes: Flow<PagingData<NoteEntity>> = Pager(PagingConfig(pageSize = 20)) {
        NotePagingSource()
    }.flow
        .cachedIn(viewModelScope)
}
