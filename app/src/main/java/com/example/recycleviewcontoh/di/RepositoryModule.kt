package com.training.pagingsample.di

import com.example.recycleviewcontoh.Repo.MovieRepository
import com.training.pagingsample.data.network.MovieAppService
import org.koin.dsl.module

val repositoryModule = module {
    single { createRepository(get()) }
}

fun createRepository(
    movieAppService: MovieAppService
) : MovieRepository = MovieRepository(movieAppService)